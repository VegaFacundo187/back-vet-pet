export default function checkRole (param) {
    return (req, res, next) => {
      const { role } = res.locals;
      if (param.hasRole.includes(role)) {
        next();
      } else {
        res.status(401).send({message: 'No tiene los permisos indicados para realizar esta operación 🐱‍👤🐱‍👤🐱‍👤'})
      }
    }
  }