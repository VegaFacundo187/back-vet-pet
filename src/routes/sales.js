import express from "express";
var router = express.Router();
import Sale from "../models/Sale";
import Product from "../models/Product";
import checkRole from "../middelwares/AuthRole";
import mongoose from "mongoose";
import mercadopago from "mercadopago"
import AuthToken from "../middelwares/AuthToken";


router.post("/addsale", AuthToken, async (req, res) => {
  let stockCheck = {
    msg: "",
    error: false
  };
  let saleSum = 0;
  const prodIds = await req.body.products.map(prod => {
    return mongoose.Types.ObjectId(prod.prodId)
  });

  Product.find({
    '_id': { $in: prodIds }
  }, async (error, prods) => {
    if (error) {
      res.send(error);
      return;
    }
    await prods.forEach((prod, index) => {
      if (!(prod.stock >= req.body.products[index].count)) {
        stockCheck.msg += `No hay suficiente stock ${prod.prodName}: El stock disponible es ${prod.stock}. `;
        stockCheck.error = true
      }
      saleSum += (prod.price * (100 - prod.discount) / 100) * req.body.products[index].count;
      console.log("Precio con descuento de :", prod.prodName, (prod.price * (100 - prod.discount) / 100))
    })
    if (stockCheck.error) {
      res.status(500).send({ msg: stockCheck.msg });
      return;
    }
    saveSale(req, res, prods, saleSum);
  })
})


function saveSale(req, res, prods, saleSum) {
  const newSale = new Sale({
    client: res.locals._id,
    products: req.body.products,
    date: Date.now(),
    totalSale: saleSum,

  });

  const prodIds = req.body.prodIds;

  newSale.save((error, sale) => {
    if (error) {
      res.send(error);
      return;
    }

    const preference = {}

    preference.items = prods.map(({ _id, prodName, price, discount }) => {

      let priceDiscount = price - ((price / 100) * discount);
      let selectedItem = req.body.products.find(item => item.prodId == _id)
      return { id: _id, title: prodName, unit_price: priceDiscount, quantity: selectedItem.count }
    })
    preference.back_urls = {
      "success": "http://localhost:3000/confirmacionCompra",
      "failure": "http://localhost:3000/confirmacionFalla",
      "pending": "http://localhost:3000/compraPendiente"
    }
    preference.auto_return = "approved";

    preference.external_reference = sale._id.toString();

    mercadopago.preferences.create(preference)
      .then(function (response) {
        // Este valor reemplazará el string "$$init_point$$" en tu HTML
        let mplink = { link: response.body.init_point }
        res.send(mplink);
      }).catch(function (error) {
        console.log(error);
      });
  });
}

router.put('/updateStock', async (req, res) => {

  let saleId;
  let prodSold;
  let statusSale = {
    approved: false,
    pending: false,
    failure: false
  };

 await mercadopago.payment.get(req.body.collection_id, {}, async function (error, response) {
    if (error) return console.log('An error ocurred: ' + error.message);
   
    console.log(response.body.status);
if(response.body.status=="approved" || response.body.status=="pending"){
    saleId = response.response.external_reference;
    await Sale.findOneAndUpdate({ _id: saleId }, { saleStatus: "done" }, { new: true }, (
      err,
      result
    ) => {
      if (err) {
        res.send(err);
        return;
      }
      prodSold = result; // a la venta obtengo en result y la cargo en prodSold
    });
    updateStock(prodSold);
    if (response.body.status == "approved") { statusSale.approved = true };
    if (response.body.status == "pending") { statusSale.pending = true };
    console.log("El status esssssssssssssssssssssssssssssss :",statusSale);
    res.send(statusSale);
    return;
  }else{
    res.status(410).send({msg:"Error en la compra"});
    return;
  }
  })
  
  return;

})

async function updateStock(prodSold) {

  await prodSold.products.forEach(async prod => {

    console.log(prod.count);
    await Product.updateOne({ _id: prod.prodId },
      { $inc: { stock: -prod.count / 2 } },
      function (err, item) {
        if (err) {
          res.send(err);
          return;
        }
        console.log(`Se restó ${prod.count} de el stock de ${prod.prodId}`);
      }
    );
  });

}

export default router;
