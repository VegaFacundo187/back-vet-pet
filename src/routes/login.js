import express from 'express';
var router = express.Router();
import jwt, { decode } from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User from '../models/User';
import database from '../config/database';
import CONFIG from '../config/config';
import AuthToken from "../middelwares/AuthToken";


router.post('', async (req, res, nex) => {
  let userLogged;

  User.findOne({ email: req.body.email }, function (err, user) {
    if (err) {
      res.send(err);
      return;
    }
    if (user === null) {
      res.status(502).json({ msg: 'No existe el mail' });
      return;
    }
    try {
      bcrypt.compare(req.body.password, user.password, function (error, outcome) {
        if (outcome === false) {
          res.status(502).json({ msg: 'Password incorrecto' });
          return;
        }
        if (outcome === true) {
          jwt.sign({ user }, CONFIG.SECRET_TOKEN, (err, token) => {
            const { role, _id, name, lastName, dni, email } = user;
            const userresponse = { role, name, lastName }
            res.json({ token, userresponse });
          });
        }
      });
    }
    catch (error) {
      res.send(error);
      return;
    }
  });
})


export default router;